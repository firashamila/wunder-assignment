module.exports = {
  testEnvironment: "jsdom",
  setupFilesAfterEnv: ["<rootDir>/enzyme.config.js"],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json"],
  testMatch: ["<rootDir>/src/**/*.test.tsx", "<rootDir>/src/**/*.spec.ts"],
  rootDir: ".",
  snapshotSerializers: ["enzyme-to-json/serializer"],
  transformIgnorePatterns: ["<rootDir>/node_modules"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  moduleNameMapper: {
    "\\.scss$": "identity-obj-proxy"
  }
};

import React from "react";
import { mount } from "enzyme";
import { registerProps } from "../../../fixtures";
import { AddressInformation } from "./AddressInformation";

it("renders correctly", () => {
  const component = mount(<AddressInformation {...registerProps} />);
  expect(component).toMatchSnapshot();
});

import React, { FunctionComponent, useState } from "react";
import { IRegisterStateProps } from "../../../containers/Register/IRegisterStateProps";
import { IRegisterDispatchProps } from "../../../containers/Register/IRegisterDispatchProps";
import { STEP } from "../../../models";
import { Input } from "../../Input/Input";
import "./addressInformation.scss";
const ERROR_MSG = "Please Fill Out all inputs";

export const AddressInformation: FunctionComponent<
  IRegisterStateProps & Pick<IRegisterDispatchProps, "setRegistration">
> = ({ setRegistration, registration }) => {
  const [city, setCity] = useState<string>("");
  const [street, setStreet] = useState<string>("");
  const [houseNumber, setHouseNumber] = useState<string>("");
  const [zipCode, setZipCode] = useState<number>(0);
  const [error, setIsError] = useState<boolean>(false);

  const handleClick = () => {
    if (city && street && houseNumber && zipCode) {
      setIsError(false);
      setRegistration({
        customer: {
          ...registration.customer,
          address: { city, street, houseNumber, zipCode }
        },
        step: STEP.THIRD
      });
    } else {
      setIsError(true);
    }
  };

  return (
    <div className="form">
      <div className={" address-grid"}>
        <Input
          className={"street"}
          label={"Street"}
          type={"text"}
          inputValue={street}
          inputTitle={"Street here"}
          onChange={setStreet}
        />
        <Input
          className={"city"}
          label={"City"}
          type={"text"}
          inputValue={city}
          onChange={setCity}
        />
        <Input
          className={"zip"}
          label={"Zip"}
          type={"number"}
          inputValue={zipCode.toString()}
          onChange={setZipCode}
        />
        <Input
          className={"house"}
          label={"House Number"}
          type={"text"}
          inputValue={houseNumber}
          onChange={setHouseNumber}
        />
      </div>

      <a className="next-button" onClick={handleClick}>
        Next
      </a>
      {error && <p className="error">{ERROR_MSG}</p>}
    </div>
  );
};

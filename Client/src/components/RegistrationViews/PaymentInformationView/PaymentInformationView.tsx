import React, { FunctionComponent, useState } from "react";
import { IRegisterStateProps } from "../../../containers/Register/IRegisterStateProps";
import { IRegisterDispatchProps } from "../../../containers/Register/IRegisterDispatchProps";
import { Input } from "../../Input/Input";
const ERROR_MSG = "Please Fill Out all inputs";

export const PaymentInformationView: FunctionComponent<
  IRegisterStateProps &
    Pick<IRegisterDispatchProps, "setRegistration" | "sendPaymentData">
> = ({ setRegistration, registration, sendPaymentData }) => {
  const [owner, setOwner] = useState<string>("");
  const [iban, setIBan] = useState<string>("");
  const [error, setIsError] = useState<boolean>(false);

  const handleClick = () => {
    if (owner && iban) {
      setIsError(false);
      setRegistration({
        ...registration,
        customer: { ...registration.customer, owner, iban }
      });
      sendPaymentData &&
        sendPaymentData({
          customerId: 7,
          iban,
          owner
        });
    } else {
      setIsError(true);
    }
  };

  return (
    <div className="form">
      <Input
        label={"Owner"}
        type={"text"}
        inputValue={owner}
        onChange={setOwner}
      />
      <Input
        label={"IBan"}
        type={"text"}
        inputValue={iban}
        onChange={setIBan}
      />
      <a className="next-button" onClick={handleClick}>
        Next
      </a>
      {error && <p className="error">{ERROR_MSG}</p>}
    </div>
  );
};

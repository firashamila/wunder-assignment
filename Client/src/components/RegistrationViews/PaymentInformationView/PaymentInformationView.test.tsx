import React from "react";
import { mount } from "enzyme";
import { registerProps } from "../../../fixtures";
import { PaymentInformationView } from "./PaymentInformationView";

it("renders correctly", () => {
  const component = mount(<PaymentInformationView {...registerProps} />);
  expect(component).toMatchSnapshot();
});

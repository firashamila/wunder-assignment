import React from "react";
import { mount } from "enzyme";
import { registerProps } from "../../../fixtures";
import { SuccessView } from "./SuccessView";

it("renders correctly", () => {
  const component = mount(<SuccessView {...registerProps} />);
  expect(component).toMatchSnapshot();
});

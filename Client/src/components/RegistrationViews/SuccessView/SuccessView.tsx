import React, { FunctionComponent } from "react";
import { ICustomer } from "../../../models";
import "./successView.scss";
import { IRegisterDispatchProps } from "../../../containers/Register/IRegisterDispatchProps";

export const SuccessView: FunctionComponent<
  Pick<ICustomer, "paymentDataId"> & Pick<IRegisterDispatchProps, "undoAll">
> = ({ paymentDataId, undoAll }) => (
  <div className="success">
    <div className="success-icon">
      <div className="success-icon__one"></div>
      <div className="success-icon__two"></div>
    </div>
    <h4 className="payment-id">{paymentDataId}</h4>
    <a className="undo-button" onClick={undoAll}>
      Undo All
    </a>
  </div>
);

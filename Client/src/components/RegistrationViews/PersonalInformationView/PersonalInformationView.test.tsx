import React from "react";
import { mount } from "enzyme";
import { registerProps } from "../../../fixtures";
import { PersonalInformationView } from "./PersonalInformationView";

it("renders correctly", () => {
  const component = mount(<PersonalInformationView {...registerProps} />);
  expect(component).toMatchSnapshot();
});

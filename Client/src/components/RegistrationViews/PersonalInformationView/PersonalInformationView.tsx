import React, { FunctionComponent, useState } from "react";
import { IRegisterStateProps } from "../../../containers/Register/IRegisterStateProps";
import { IRegisterDispatchProps } from "../../../containers/Register/IRegisterDispatchProps";
import { Input } from "../../Input/Input";
import { STEP } from "../../../models";

const ERROR_MSG = "Please Fill Out all inputs";

export const PersonalInformationView: FunctionComponent<
  IRegisterStateProps & Pick<IRegisterDispatchProps, "setRegistration">
> = ({ setRegistration, registration }) => {
  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setSecondName] = useState<string>("");
  const [error, setIsError] = useState<boolean>(false);

  const handleClick = () => {
    if (firstName && lastName) {
      setIsError(false);
      setRegistration({
        customer: { ...registration.customer, firstName, lastName },
        step: STEP.SECOND
      });
    } else {
      setIsError(true);
    }
  };
  return (
    <div className="form">
      <Input
        label={"first Name"}
        type={"text"}
        inputValue={firstName}
        onChange={setFirstName}
      />
      <Input
        label={"Second Name"}
        type={"text"}
        inputValue={lastName}
        onChange={setSecondName}
      />
      <a className="next-button" onClick={handleClick}>
        Next
      </a>
      {error && <p className="error">{ERROR_MSG}</p>}
    </div>
  );
};

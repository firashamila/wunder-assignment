import React from "react";
import { FunctionComponent } from "react";
import { ProgressBar } from "./ProgressBar";
import { RollingScooter } from "./RollingScooter";
import { STEP } from "../../models";

export const StepsLoader: FunctionComponent<{ step: STEP }> = ({ step }) => (
  <div className="steps-loader">
    <RollingScooter step={step}>
      <img
        src={`https://www.pinclipart.com/picdir/big/179-1795901_kick-scooter-png-pic-kick-scooter-icon-png.png`}
      />
    </RollingScooter>
    <ProgressBar step={step}>
      <span className="bar">
        <span className="progress"></span>
      </span>
    </ProgressBar>
  </div>
);

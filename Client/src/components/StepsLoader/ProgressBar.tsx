import styled, { css } from "styled-components";
import { STEP } from "../../models";
import { calculateProgressPercentage } from "../../utils";
import {
  easeInOutTransition,
  barRadius,
  barBg,
  barSize,
  primaryBlue
} from "../../utils/styleUtils";

export const ProgressBar = styled.div.attrs({
  className: "progress-bar"
})<{ step: STEP }>`
  border-radius: ${barRadius};
  overflow: hidden;
  width: 100%;
  span {
    display: block;
  }
  .bar {
    background: ${barBg};
  }
  .progress {
    animation: loader 8s ease forwards;
    background: ${primaryBlue};
    color: #fff;
    padding: ${barSize};
    width: 4%;
    ${({ step }) =>
      css`
        width: ${calculateProgressPercentage(step)}%;
      `}
    ${easeInOutTransition(400)}
  }
`;

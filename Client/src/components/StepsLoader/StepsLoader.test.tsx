import React from "react";
import { mount } from "enzyme";
import { registerProps } from "../../fixtures";
import { StepsLoader } from "./StepsLoader";
import { STEP } from "../../models";

it("renders correctly", () => {
  const component = mount(<StepsLoader step={STEP.FOURTH} />);
  expect(component).toMatchSnapshot();
});

import styled, { css } from "styled-components";
import { calculateProgressPercentage } from "../../utils";
import { STEP } from "../../models";
import { easeInOutTransition } from "../../utils/styleUtils";

export const RollingScooter = styled.div.attrs({
  className: "rolling-scooter"
})<{ step: STEP }>`
  img {
    width: 8%;
    height: 8%;
    box-sizing: unset;
    ${({ step }) =>
      css`
        padding-left: ${calculateProgressPercentage(step) - 6}%;
      `}
    ${easeInOutTransition(600)}
  }
`;

import React, { Component, Fragment } from "react";
import { IRegisterStateProps } from "../../containers/Register/IRegisterStateProps";
import { IRegisterDispatchProps } from "../../containers/Register/IRegisterDispatchProps";
import { StepsLoader } from "../StepsLoader/StepsLoader";
import { PersonalInformationView } from "../RegistrationViews/PersonalInformationView/PersonalInformationView";
import { PaymentInformationView } from "../RegistrationViews/PaymentInformationView/PaymentInformationView";
import { SuccessView } from "../RegistrationViews/SuccessView/SuccessView";
import { AddressInformation } from "../RegistrationViews/AddressInformationView/AddressInformation";
import { STEP } from "../../models";
import "./register.scss";

export class Register extends Component<
  IRegisterStateProps & IRegisterDispatchProps
> {
  public componentDidMount = () => {
    const { initRegistrationFromLocalStorage } = this.props;
    initRegistrationFromLocalStorage && initRegistrationFromLocalStorage();
  };
  public render() {
    const {
      setRegistration,
      registration,
      sendPaymentData,
      undoAll
    } = this.props;
    const { step, customer } = registration;
    const returnView = () => {
      switch (step) {
        case STEP.FIRST:
          return (
            <PersonalInformationView
              registration={registration}
              setRegistration={setRegistration}
            />
          );
        case STEP.SECOND:
          return (
            <AddressInformation
              registration={registration}
              setRegistration={setRegistration}
            />
          );
        case STEP.THIRD:
          return (
            <PaymentInformationView
              registration={registration}
              setRegistration={setRegistration}
              sendPaymentData={sendPaymentData}
            />
          );
        case STEP.FOURTH:
          return (
            <SuccessView
              undoAll={undoAll}
              paymentDataId={customer.paymentDataId}
            />
          );
      }
    };
    return (
      <div className="register-form">
        <StepsLoader step={step} />
        <div className="form-heading">
          {step === STEP.FOURTH ? (
            <Fragment>
              <h2>You Did it!</h2>
              <h3>Here is your PaymentId </h3>
            </Fragment>
          ) : (
            <Fragment>
              <h2>DO it!</h2>
              <h3>SignUp and Unlock your first scooter</h3>
            </Fragment>
          )}
        </div>
        {returnView()}
      </div>
    );
  }
}

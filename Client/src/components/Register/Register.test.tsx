import React from "react";
import { mount } from "enzyme";
import { registerProps } from "../../fixtures";
import { Register } from "./Register";

it("renders correctly", () => {
  const component = mount(<Register {...registerProps} />);
  expect(component).toMatchSnapshot();
});

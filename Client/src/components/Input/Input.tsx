import React, { FunctionComponent } from "react";
import { IInputProps } from "./IInputProps";
import "./Input.scss";

export const Input: FunctionComponent<IInputProps> = ({
  type,
  inputValue,
  inputTitle,
  label,
  className,
  onChange
}) => (
  <div className={`form-group ${className} ${inputValue ? "active" : ""}`}>
    <label className="form-label">{label}</label>
    <input
      className="form-input"
      type={type}
      title={inputTitle}
      value={inputValue}
      onChange={e => onChange(e.target.value)}
    />
    <div className="bottom-border"></div>
  </div>
);

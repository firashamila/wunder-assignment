export interface IInputProps {
  type: "text" | "number";
  inputValue?: string;
  inputTitle?: string;
  label: string;
  className?: string;
  onChange: (e: any) => void;
}

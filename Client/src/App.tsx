import React, { FunctionComponent } from "react";
import { RegisterContainer } from "./containers/Register/RegisterContainer";
import "./styling/index.scss";
export const App: FunctionComponent = () => <RegisterContainer />;

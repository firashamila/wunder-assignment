import { IRegisterStateProps } from "../containers/Register/IRegisterStateProps";
import { IRegisterDispatchProps } from "../containers/Register/IRegisterDispatchProps";
import { registrationInitialState } from "../store/reducers/registrationReducer";
import { noop } from "../utils";

export const registerProps: IRegisterStateProps & IRegisterDispatchProps = {
  registration: registrationInitialState,
  setRegistration: noop,
  sendPaymentData: noop,
  initRegistrationFromLocalStorage: noop,
  undoAll: noop
};

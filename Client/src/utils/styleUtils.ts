import { css } from "styled-components";

//colors
export const primaryBlue = "#2781ff";

// loading Bar variables
export const barSize = "5px";
export const barRadius = "60px";
export const barBg = "rgba(0, 0, 0, 0.075)";

export const easeInOutTransition = (time: number) => css`
  -webkit-transition: all ${time}ms ease-in-out;
  -moz-transition: all ${time}ms ease-in-out;
  -o-transition: all ${time}ms ease-in-out;
  transition: all ${time}ms ease-in-out;
`;

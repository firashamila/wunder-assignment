import { IRegistrationState, STEP } from "../models";

const LOCAL_STORAGE_REGISTRATION = "localStorageBasket";

export const getLocalStorageRegistration = (): IRegistrationState =>
  JSON.parse(localStorage.getItem(LOCAL_STORAGE_REGISTRATION) || "{}");

export const setLocalStorageRegistration = (
  registration: IRegistrationState
): void => {
  try {
    localStorage.setItem(
      LOCAL_STORAGE_REGISTRATION,
      JSON.stringify(registration)
    );
  } catch (error) {
    console.log("setLocalStorageRegistration", Error(error));
  }
};

export const setLocalStorageUserPaymentID = (paymentDataId: string): void => {
  try {
    const registration: IRegistrationState = getLocalStorageRegistration();
    setLocalStorageRegistration({
      ...registration,
      customer: { ...registration.customer, paymentDataId },
      step: STEP.FOURTH
    });
  } catch (error) {
    console.log("setLocalStorageRegistrationPaymentID", Error(error));
  }
};

export const deleteLocalStorageRegistration = (): void => {
  try {
    localStorage.removeItem(LOCAL_STORAGE_REGISTRATION);
  } catch (error) {
    console.log("deleteLocalStorageRegistration", Error(error));
  }
};

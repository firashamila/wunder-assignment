import { calculateProgressPercentage } from "./calculateProgressPercentage";
import { STEP } from "../../models";

test("should return true given internal link", () => {
  expect(calculateProgressPercentage(STEP.SECOND)).toBe(45);
});

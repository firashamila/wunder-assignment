import { STEP } from "../../models";

export const calculateProgressPercentage = (step: STEP): number => {
  switch (step) {
    case STEP.FIRST:
      return 15;
    case STEP.SECOND:
      return 45;
    case STEP.THIRD:
      return 70;
    case STEP.FOURTH:
      return 100;
  }
};

export const getPaymentAPIRoute = () =>
  `https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data`;

export const getPaymentMiddlewareRoute = () =>
  `http://localhost:5002/middleware/post/paymentInfo`;

export const isEmpty = (obj: Object) =>
  Object.entries(obj).length === 0 && obj.constructor === Object;

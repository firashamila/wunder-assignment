import { Dispatch, Action } from "redux";
import { IRegisterDispatchProps } from "./IRegisterDispatchProps";
import {
  actionSetRegistration,
  actionInitRegistrationFromLocalStorage,
  actionSendPaymentData,
  undoAll
} from "../../store/actions";
import { connect } from "react-redux";
import { Register } from "../../components";
import { IRootReducer } from "../../store/reducers";
import { IRegisterStateProps } from "./IRegisterStateProps";

const mapStateToProps = ({
  registration
}: IRootReducer): IRegisterStateProps => ({
  registration
});

const mapDispatchToProps = (
  dispatch: Dispatch<Action>
): IRegisterDispatchProps => ({
  setRegistration: registration =>
    dispatch(actionSetRegistration(registration)),
  initRegistrationFromLocalStorage: () =>
    dispatch(actionInitRegistrationFromLocalStorage()),
  sendPaymentData: paymentData => dispatch(actionSendPaymentData(paymentData)),
  undoAll: () => dispatch(undoAll())
});

export const RegisterContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);

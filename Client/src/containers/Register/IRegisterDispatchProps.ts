import { IRegistrationState, IAction, IPaymentData } from "../../models";
import { Action } from "redux";

export interface IRegisterDispatchProps {
  setRegistration: (
    registration: IRegistrationState
  ) => IAction<IRegistrationState>;
  initRegistrationFromLocalStorage?: () => Action;
  sendPaymentData: (paymentData: IPaymentData) => IAction<IPaymentData>;
  undoAll: () => Action;
}

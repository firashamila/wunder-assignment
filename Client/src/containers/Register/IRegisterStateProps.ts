import { IRegistrationState } from "../../models";

export interface IRegisterStateProps {
  registration: IRegistrationState;
}

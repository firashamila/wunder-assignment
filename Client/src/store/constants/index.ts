export const INIT_REGISTRATION_FROM_LOCAL_STORAGE =
  "initRegistrationFromLocalStorage";
export const SAVE_REGISTRATION_TO_LOCAL_STORAGE =
  "saveRegistrationToLocalStorage";
export const SET_REGISTRATION = "setRegistration";
export const SEND_PAYMENT_DATA = "sendPaymentData";
export const SET_CUSTOMER_PAYMENT_ID = "setCustomerPaymentID";
export const UNDO_ALL = "undoALL";

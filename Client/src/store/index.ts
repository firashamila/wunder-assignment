import { createStore, applyMiddleware } from "redux";
import rootReducers, { rootInitialState } from "./reducers";
import { createEpicMiddleware } from "redux-observable";
import { composeWithDevTools } from "redux-devtools-extension";
import { rootEpics } from "./epics";

const epicMiddleware = createEpicMiddleware();
export const store = createStore(
  rootReducers,
  rootInitialState,
  composeWithDevTools(applyMiddleware(epicMiddleware))
);

// redux rxjs observables side effects epics
epicMiddleware.run(rootEpics);

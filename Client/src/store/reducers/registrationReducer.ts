import { IRegistrationState, IAction, ICustomer, STEP } from "../../models";
import {
  SET_REGISTRATION,
  SET_CUSTOMER_PAYMENT_ID,
  UNDO_ALL
} from "../constants";

export const registrationInitialState: IRegistrationState = {
  customer: {
    firstName: "",
    lastName: "",
    address: {
      city: "",
      street: "",
      houseNumber: "",
      zipCode: 0
    },
    owner: "",
    iban: ""
  },
  step: STEP.FIRST
};

export const registrationReducer = (
  state: IRegistrationState = registrationInitialState,
  action: IAction<any>
): IRegistrationState => {
  switch (action.type) {
    case SET_REGISTRATION:
      return action.payload;
    case SET_CUSTOMER_PAYMENT_ID:
      return {
        ...state,
        customer: { ...state.customer, paymentDataId: action.payload },
        step: STEP.FOURTH
      };
    case UNDO_ALL:
      return registrationInitialState;
    default:
      return state;
  }
};

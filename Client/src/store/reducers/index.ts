import { combineReducers } from "redux";
import {
  registrationReducer,
  registrationInitialState
} from "./registrationReducer";

import { IRegistrationState } from "../../models";

export interface IRootReducer {
  registration: IRegistrationState;
}

// used to setInitial state for all reducers
export const rootInitialState = {
  registration: registrationInitialState
};

export default combineReducers({
  registration: registrationReducer
});

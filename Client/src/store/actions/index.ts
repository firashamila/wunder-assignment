import { Action } from "redux";
import {
  IAction,
  IRegistrationState,
  IPaymentData,
  ICustomer
} from "../../models";
import {
  INIT_REGISTRATION_FROM_LOCAL_STORAGE,
  SET_REGISTRATION,
  SEND_PAYMENT_DATA,
  SET_CUSTOMER_PAYMENT_ID,
  UNDO_ALL
} from "../constants";

export const actionInitRegistrationFromLocalStorage = (): Action => ({
  type: INIT_REGISTRATION_FROM_LOCAL_STORAGE
});

export const actionSetRegistration = (
  payload: IRegistrationState
): IAction<IRegistrationState> => ({
  type: SET_REGISTRATION,
  payload
});

export const actionSendPaymentData = (
  payload: IPaymentData
): IAction<IPaymentData> => ({
  type: SEND_PAYMENT_DATA,
  payload
});

export const actionSetCustomerPaymentId = (
  payload: string
): IAction<string> => ({
  type: SET_CUSTOMER_PAYMENT_ID,
  payload
});

export const undoAll = (): Action => ({
  type: UNDO_ALL
});

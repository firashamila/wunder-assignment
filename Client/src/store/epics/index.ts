import {
  tap,
  ignoreElements,
  map,
  filter,
  switchMap,
  catchError
} from "rxjs/operators";

import { combineEpics, ofType } from "redux-observable";
import { ajax } from "rxjs/ajax";

import {
  SET_REGISTRATION,
  INIT_REGISTRATION_FROM_LOCAL_STORAGE,
  SEND_PAYMENT_DATA,
  UNDO_ALL
} from "../constants";

import {
  setLocalStorageRegistration,
  getLocalStorageRegistration,
  isEmpty,
  setLocalStorageUserPaymentID,
  deleteLocalStorageRegistration
} from "../../utils";
import { IRegistrationState } from "../../models";
import { actionSetRegistration, actionSetCustomerPaymentId } from "../actions";
import { getPaymentMiddlewareRoute } from "../../utils/ApiRoute";
import { of } from "rxjs";

const handleSetRegistrationEpic = (action$: any) =>
  action$.pipe(
    ofType(SET_REGISTRATION),
    tap(({ payload: registration }: { payload: IRegistrationState }) => {
      setLocalStorageRegistration(registration);
    }),
    ignoreElements()
  );

const handleInitRegistrationFromLocalStorageEpic = (action$: any) =>
  action$.pipe(
    ofType(INIT_REGISTRATION_FROM_LOCAL_STORAGE),
    filter(() => !isEmpty(getLocalStorageRegistration())),
    map(() => actionSetRegistration(getLocalStorageRegistration()))
  );

const sendPaymentInfoEpic = (action$: any) =>
  action$.pipe(
    ofType(SEND_PAYMENT_DATA),
    switchMap(({ payload: paymentData }) =>
      ajax.post(getPaymentMiddlewareRoute(), paymentData)
    ),
    catchError(err => {
      console.error(`[POST-PAYMENT-EPIC-ERROR] : ${err}`);
      return of({ data: [] });
    }),
    map(
      ({
        response: { paymentDataId }
      }: {
        response: { paymentDataId: string };
      }) => {
        setLocalStorageUserPaymentID(paymentDataId);
        return actionSetCustomerPaymentId(paymentDataId);
      }
    )
  );

const undoALlEpic = (action$: any) =>
  action$.pipe(
    ofType(UNDO_ALL),
    tap(() => {
      deleteLocalStorageRegistration();
    }),
    ignoreElements()
  );

export const rootEpics = combineEpics(
  handleSetRegistrationEpic,
  handleInitRegistrationFromLocalStorageEpic,
  sendPaymentInfoEpic,
  undoALlEpic
);

import { ICustomer } from "./ICustomer";
import { STEP } from "./STEP";

export interface IRegistrationState {
  customer: ICustomer;
  step: STEP;
}

export interface IPaymentData {
  customerId: number;
  iban: string;
  owner: string;
}

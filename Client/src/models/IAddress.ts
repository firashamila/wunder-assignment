export interface IAddress {
  city: string;
  street: string;
  houseNumber: string;
  zipCode: number;
}

import { IAddress } from "./IAddress";

export interface ICustomer {
  firstName: string;
  lastName: string;
  address: IAddress;
  owner: string;
  iban: string;
  paymentDataId?: string;
}

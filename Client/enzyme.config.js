const enzyme = require("enzyme");
const Adapter = require("enzyme-adapter-react-16");
// to setup enzyme to use React 16
enzyme.configure({ adapter: new Adapter() });

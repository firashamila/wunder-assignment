# Project tree

```bash
.
├── Client # the frontend part
│   ├── public #output of building the frontend app 
│   └── src 
│       ├── assets #images and icons
│       ├── components   # React UI components, components are not connected to the state,    
│       │   ├── Input      #Styled input used in  all registering views 
│       │   ├── Register  #main customer registering component, decides which view should apear to the user
│       │   ├── RegistrationViews #React UI components each for a specific View
│       │   │   ├── AddressInformationView
│       │   │   ├── PaymentInformationView
│       │   │   ├── PersonalInformationView
│       │   │   └── SuccessView
│       │   └── StepsLoader #Loader  component styled using styled-components 
│       ├── containers #a container is a connection between UI components and the store
│       │   └── Register
│       ├── fixtures # used for unit testing
│       ├── models # Client typescript models
│       ├── store # magic is here , using FLUX architecture alongside with side effects management using observables middleware 
│       │   ├── actions # triggered from  components
│       │   ├── constants # store constants ( for consistency )
│       │   ├── epics # side effects
│       │   └── reducers # state chunks 
│       ├── styling # scss styles
│       └── utils
└── Middleware  #  receiving post request from the Client and firing post request to the demo API
    ├── build # build output comes here
    ├── src
    │   ├── controller 
    │   ├── server
    │   └── index
    └── utils
```


# Install Deps
install dependencies **in three places** : rootDir, Middleware and  Client

# Run in Dev Mode: methods
- **run both projects**, in rootDirectory:  `$ npm run start:dev`
- run **Client** : in Client: `$ npm run start:dev`
- run **Middleware** : in Middleware: `$ npm run start:dev`
  
# Build: methods
- build **both projects**, in rootDirectory:  `$ npm run build`
- build **Client** only: in Client: `$ npm run build`
- build **Middleware** only ,in Middleware: `$ npm run build`

# Run in Production mode
- open the  **Client** : open `~/Client/public/index.html`  in the browser
- run the  **Middleware** in Middleware: `$ npm run start:prod`

# Unit Test
Snapshot testing is implemented using enzyme and jest for React components and util functions.
- start tests: `$ npm run test`
- update snapShots: `$ npm run test:update`
- watch tests: `$ npm run test:watch`


# Hooks
 before commit hook is implemented to run the Client tests and fails if a test fail.


# tech talk 

### Styled-components
styled-components is used in order to build a React loader component that its style depends on its internal state by passing props to the styled component.


### redux-observables
a cool way to manage side effects is an efficient manner, action functions are converted to lists of observable objects to which we subscribe and manage side effects,
all logic that can be extracted from the state reducer is here. 

### build 
all build configurations are built from scratch , step by step ( you dont get the chance to do it always ) , using webpack to build both the middleware server and the frontend client application

### Styling
SCSS styling is also implemented from scratch, decided not to use any library.

### why introducing a Middleware  ?
well, when testing the demo api it was failing on chrome and loaded with warnings in mozilla, as it seems chrome added more security layers and failing cors missing configurations requests on the client,
Also the demo API is returning 400 on any header parameter adding .
As in node server the post request does not fail came the idea to introduce a middleware that requests the demo API and sends back the response to the React client


### architectural pattern choice : Flux
for React , Flux proved its efficiency, since react is not bi-direction in order to avoid loop-holes and error pron sources, Flux also is bi Bidirectional.
redux is following the flux pattern with a small change . 
when a user interacts with the View => 
- actions are dispatched, 
- actions hit the store  + side effects for specific actions
- store state changes 
- the view changes since it is depending on the store global state 



### If I had more Time ( 3 more days )
- add unit tests to the server Middleware
- add more unit tests to the React application that tests the user interaction components behavior
- add a subscription for scooters + cars
- add a user profile 
- add Rides list to the user profile
- enhance the SCSS structure 

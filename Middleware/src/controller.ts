import { OK, BAD_REQUEST } from "http-status-codes";
import { Logger } from "@overnightjs/logger";
import { Request, Response } from "express";
import { getPaymentAPIRoute } from "../../Client/src/utils";
import axios from "axios";

export const postPaymentData = async (req: Request, res: Response) => {
  const SUCCESS_MSG = "Requesting Api and sending back result ... ";
  try {
    let result = await axios.post(getPaymentAPIRoute(), req.body);
    Logger.Info(SUCCESS_MSG);
    return res.status(OK).json({
      paymentDataId: result.data.paymentDataId
    });
  } catch (err) {
    Logger.Err(err, true);
    return res.status(BAD_REQUEST).json({
      error: err.message
    });
  }
};

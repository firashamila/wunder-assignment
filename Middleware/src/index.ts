import express from "express";
import { Server } from "./server";

const applicationServer = express();
export const server = new Server(applicationServer);
server.start(5002);
